package fr.afia_competitions.drivetogaether.legorobot;

import java.util.List;
import java.util.LinkedList;

public class Instruction {
    public enum Direction {
        LEFT,
        RIGHT,
        STRAIGHT,
        UTURN;
    }
    public enum Action {
        TAKE_VICTIM,
        DROP_VICTIM;
    }

    private Direction direction = null;
    private List<Action> actions = new LinkedList<>();

    public Instruction ( String action ) {
        for ( int i = 0, n = action.length(); i < n; i++ ) {
            switch ( action.charAt ( i ) ) {
            case 'l':
                this.direction=Direction.LEFT;
                break;
            case 'r':
                this.direction=Direction.RIGHT;
                break;
            case 's':
                this.direction=Direction.STRAIGHT;
                break;
            case 'u':
                this.direction=Direction.UTURN;
                break;
            case 't':
                this.actions.add ( Action.TAKE_VICTIM );
                break;
            case 'd':
                this.actions.add ( Action.DROP_VICTIM );
                break;

            }
        }
    }

    public boolean hasDirection() {
        return this.direction != null;
    }

    public Direction getDirection() {
        return this.direction;
    }

    public void setDirection ( Direction d ) {
        this.direction = d;
    }

    public boolean hasActions() {
        return this.actions != null;
    }

    public List<Action> getActions() {
        return this.actions;
    }

    public void addAction ( Action a ) {
        this.actions.add ( a );
    }

    public String toString() {
        String str = "";
        if ( this.direction != null ) {
            switch ( this.direction ) {
            case LEFT:
                str = "l";
                break;
            case RIGHT:
                str = "r";
                break;
            case STRAIGHT:
                str = "s";
                break;
            case UTURN:
                str = "u";
                break;
            }
        }
        if ( this.actions != null ) {
            for ( int i = 0, n = this.actions.size(); i < n; i++ ) {
                switch ( this.actions.get ( i ) ) {
                case TAKE_VICTIM:
                    str += "t";
                    break;
                case DROP_VICTIM:
                    str += "d";
                    break;
                }
            }
        }

        return str;
    }
}
